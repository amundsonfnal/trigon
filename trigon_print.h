#ifndef TRIGON_PRINT_H
#define TRIGON_PRINT_H
#include "trigon.h"

#include <ostream>

template <typename T, unsigned int Power, unsigned int Dim>
void
print_coeffs(Trigon<T, Power, Dim> const& t, std::ostream& out,
             bool suppress_zeroes = false)
{
    print_coeffs(t.lower, out, suppress_zeroes);
    auto the_indices(indices<Power, Dim>());
    for (size_t i = 0; i < t.terms.size(); ++i) {
        if ((!suppress_zeroes) || (t.terms[i] != 0.0)) {
            out << "{";
            for (size_t j = 0; j < the_indices[i].size(); ++j) {
                out << the_indices[i][j];
                if (j < the_indices[i].size() - 1) {
                    out << ", ";
                }
            }
            out << "} : ";
            out << t.terms[i] << "\n";
        }
    }
}

template <typename T, unsigned int Dim>
void print_coeffs(Trigon<T, 0, Dim> const& t, std::ostream& out,
                  bool suppress_zeroes = false)
{
    out << "{}: " << t.terms[0] << "\n";
}

#endif // TRIGON_PRINT_H
