#include "poly.h"
#include "gtest/gtest.h"
#include <cmath>

namespace {

const double tolerance = 1.0e-14;

TEST(Poly_test, construct)
{
    Poly poly;

    EXPECT_EQ(poly.c, 0.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], 0.0);
    }
}

TEST(Poly_test, copy)
{
    Poly poly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly roly(poly);

    EXPECT_EQ(poly.c, roly.c);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], roly.d[i]);
    }
}

TEST(Poly_test, from_double)
{
    Poly poly(37.0);

    EXPECT_EQ(poly.c, 37.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], 0.0);
    }
}

TEST(Poly_test, assignment)
{
    Poly poly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly roly;
    roly = poly;

    EXPECT_EQ(poly.c, roly.c);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], roly.d[i]);
    }
}

TEST(Poly_test, assignment_double)
{
    Poly poly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    poly = 37.0;

    EXPECT_EQ(poly.c, 37.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], 0.0);
    }
}

TEST(Poly_test, plus_equals)
{
    Poly poly, roly;

    poly.c = 1.0;
    roly.c = 10.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
        roly.d[i] = i + 10.0;
    }

    poly += roly;

    EXPECT_EQ(poly.c, 11.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], 2 * i + 11.0);
    }
}

TEST(Poly_test, plus_equals_double)
{
    Poly poly, roly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    poly += 10.0;

    EXPECT_EQ(poly.c, 11.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], i + 1.0);
    }
}

TEST(Poly_test, plus)
{
    Poly poly, roly;

    poly.c = 1.0;
    roly.c = 10.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
        roly.d[i] = i + 10.0;
    }

    Poly oly;
    oly = roly + poly;

    EXPECT_EQ(oly.c, 11.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], 2 * i + 11.0);
    }
}

TEST(Poly_test, plus_double)
{
    Poly poly, roly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = poly + 10.0;

    EXPECT_EQ(oly.c, 11.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], i + 1.0);
    }
}

TEST(Poly_test, other_plus_double)
{
    Poly poly, roly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = 10.0 + poly;

    EXPECT_EQ(oly.c, 11.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], i + 1.0);
    }
}

TEST(Poly_test, unary_minus)
{
    Poly poly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly roly;
    roly = -poly;

    EXPECT_EQ(-poly.c, roly.c);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(-poly.d[i], roly.d[i]);
    }
}

TEST(Poly_test, minus_equals)
{
    Poly poly, roly;

    poly.c = 10.0;
    roly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 2 * i + 10.0;
        roly.d[i] = i + 1.0;
    }

    poly -= roly;

    EXPECT_EQ(poly.c, 9.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], i + 9.0);
    }
}

TEST(Poly_test, minus_equals_double)
{
    Poly poly, roly;

    poly.c = 10.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    poly -= 1.0;

    EXPECT_EQ(poly.c, 9.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], i + 1.0);
    }
}

TEST(Poly_test, minus)
{
    Poly poly, roly;

    poly.c = 10.0;
    roly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 2 * i + 10.0;
        roly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = poly - roly;

    EXPECT_EQ(oly.c, 9.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], i + 9.0);
    }
}

TEST(Poly_test, minus_double)
{
    Poly poly, roly;

    poly.c = 10.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = poly - 1.0;

    EXPECT_EQ(oly.c, 9.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], i + 1.0);
    }
}

TEST(Poly_test, other_minus_double)
{
    Poly poly, roly;

    poly.c = 1.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = 10.0 - poly;

    EXPECT_EQ(oly.c, 9.0);
    for (unsigned int i = 0; i < 6; ++i) {
        int int_i(i);
        EXPECT_EQ(oly.d[i], -int_i - 1.0);
    }
}

TEST(Poly_test, times_equals)
{
    Poly poly, roly;

    poly.c = 2.0;
    roly.c = 10.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
        roly.d[i] = i + 10.0;
    }

    poly *= roly;

    EXPECT_EQ(poly.c, 20.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], 2.0 * roly.d[i] + (i + 1.0) * roly.c);
    }
}

TEST(Poly_test, times_equals_double)
{
    Poly poly;

    poly.c = 2.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    poly *= 7.0;

    EXPECT_EQ(poly.c, 14.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], (i + 1.0) * 7.0);
    }
}

TEST(Poly_test, times)
{
    Poly poly, roly;

    poly.c = 2.0;
    roly.c = 10.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
        roly.d[i] = i + 10.0;
    }

    Poly oly;
    oly = poly * roly;

    EXPECT_EQ(oly.c, 20.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], poly.c * roly.d[i] + poly.d[i] * roly.c);
    }
}

TEST(Poly_test, times_double)
{
    Poly poly;

    poly.c = 2.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = poly * 7.0;

    EXPECT_EQ(oly.c, 14.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], poly.d[i] * 7.0);
    }
}

TEST(Poly_test, times_double_other)
{
    Poly poly;

    poly.c = 2.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = 7.0 * poly;

    EXPECT_EQ(oly.c, 14.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], poly.d[i] * 7.0);
    }
}

TEST(Poly_test, div_equals)
{
    Poly poly, roly;

    poly.c = 4.0;
    roly.c = 2.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
        roly.d[i] = i + 10.0;
    }

    poly /= roly;

    EXPECT_EQ(poly.c, 2.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], ((i + 1.0) * 2.0 - (i + 10) * 4.0) / (2.0 * 2.0));
    }
}

TEST(Poly_test, div_equals_double)
{
    Poly poly;

    poly.c = 4.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    poly /= 2.0;

    EXPECT_EQ(poly.c, 2.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(poly.d[i], (i + 1.0) / 2.0);
    }
}

TEST(Poly_test, div_poly) // "div" is reserved
{
    Poly poly, roly;

    poly.c = 4.0;
    roly.c = 2.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
        roly.d[i] = i + 10.0;
    }

    Poly oly;
    oly = poly / roly;

    EXPECT_EQ(oly.c, 2.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], ((i + 1.0) * 2.0 - (i + 10) * 4.0) / (2.0 * 2.0));
    }
}

TEST(Poly_test, div_double)
{
    Poly poly;

    poly.c = 4.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly oly;
    oly = poly / 2.0;

    EXPECT_EQ(oly.c, 2.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], (i + 1.0) / 2.0);
    }
}

TEST(Poly_test, div_double_other)
{
    Poly poly;

    poly.c = 2.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 10.0;
    }

    Poly oly;
    oly = 4.0 / poly;

    EXPECT_EQ(oly.c, 2.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], 0.0 - (i + 10));
    }
}

TEST(Poly_test, div_roundtrip)
{
    Poly poly;

    poly.c = 4.0;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = i + 1.0;
    }

    Poly roly(poly);
    Poly oly;
    oly = poly / roly;

    EXPECT_EQ(oly.c, 1.0);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_EQ(oly.d[i], 0.0);
    }
}

#include <iostream>
TEST(Poly_test, acos_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = acos(poly);

    EXPECT_EQ(roly.c, std::acos(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::acos(poly.c + 0.5 * eps) - std::acos(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, asin_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = asin(poly);

    EXPECT_EQ(roly.c, std::asin(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::asin(poly.c + 0.5 * eps) - std::asin(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, atan_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = atan(poly);

    EXPECT_EQ(roly.c, std::atan(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::atan(poly.c + 0.5 * eps) - std::atan(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, cos_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = cos(poly);

    EXPECT_EQ(roly.c, std::cos(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf = std::cos(poly.c + 0.5 * eps) - std::cos(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, cosh_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = cosh(poly);

    EXPECT_EQ(roly.c, std::cosh(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::cosh(poly.c + 0.5 * eps) - std::cosh(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, exp_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = exp(poly);

    EXPECT_EQ(roly.c, std::exp(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf = std::exp(poly.c + 0.5 * eps) - std::exp(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, log_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = log(poly);

    EXPECT_EQ(roly.c, std::log(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf = std::log(poly.c + 0.5 * eps) - std::log(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, log10_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = log10(poly);

    EXPECT_EQ(roly.c, std::log10(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::log10(poly.c + 0.5 * eps) - std::log10(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, pow_double_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    const double y = 2.4;
    Poly roly = pow(poly, y);

    EXPECT_EQ(roly.c, std::pow(poly.c, y));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::pow(poly.c + 0.5 * eps, y) - std::pow(poly.c - 0.5 * eps, y);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, pow_int_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    const int n = 3;
    Poly roly = pow(poly, n);

    EXPECT_EQ(roly.c, std::pow(poly.c, n));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::pow(poly.c + 0.5 * eps, n) - std::pow(poly.c - 0.5 * eps, n);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, sin_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = sin(poly);

    EXPECT_EQ(roly.c, std::sin(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf = std::sin(poly.c + 0.5 * eps) - std::sin(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, sinh_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = sinh(poly);

    EXPECT_EQ(roly.c, std::sinh(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::sinh(poly.c + 0.5 * eps) - std::sinh(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, sqrt_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = sqrt(poly);

    EXPECT_EQ(roly.c, std::sqrt(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::sqrt(poly.c + 0.5 * eps) - std::sqrt(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, tan_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = tan(poly);

    EXPECT_EQ(roly.c, std::tan(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf = std::tan(poly.c + 0.5 * eps) - std::tan(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

TEST(Poly_test, tanh_test)
{
    Poly poly;

    poly.c = 0.5;
    for (unsigned int i = 0; i < 6; ++i) {
        poly.d[i] = 1.0;
    }

    Poly roly = tanh(poly);

    EXPECT_EQ(roly.c, std::tanh(poly.c));

    const double eps = 1.0e-6;
    const double tolerance = 5.0e-8;
    double deltaf =
        std::tanh(poly.c + 0.5 * eps) - std::tanh(poly.c - 0.5 * eps);
    for (unsigned int i = 0; i < 6; ++i) {
        EXPECT_NEAR(roly.d[i] / poly.d[i], deltaf / eps, tolerance);
    }
}

} // namespace

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
