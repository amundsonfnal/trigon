#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "trigon.h"
#include <algorithm>
#include <cmath>

const double tolerance = 1.0e-14;

TEST_CASE("construct0", "Trigon_test")
{
    Trigon<double, 0, 3> trigon;

    REQUIRE(trigon.terms.size() == 1);
    REQUIRE(trigon.terms[0] == 0.0);
}

TEST_CASE("construct1", "Trigon_test")
{
    Trigon<double, 1, 3> trigon;

    REQUIRE(trigon.terms.size() == 3);
    std::for_each(trigon.terms.begin(), trigon.terms.end(),
                  [](double& t) { REQUIRE(t == 0.0); });
}

TEST_CASE("construct2", "Trigon_test")
{
    Trigon<double, 2, 6> trigon;

    REQUIRE(trigon.terms.size() == 21);
    std::for_each(trigon.terms.begin(), trigon.terms.end(),
                  [](double& t) { REQUIRE(t == 0.0); });
}

TEST_CASE("construct_value", "Trigon_test")
{
    double val = 7.0;
    Trigon<double, 2, 6> trigon(val);

    REQUIRE(trigon.terms.size() == 21);
    std::for_each(trigon.terms.begin(), trigon.terms.end(),
                  [](double& t) { REQUIRE(t == 0.0); });
    std::for_each(trigon.lower.terms.begin(), trigon.lower.terms.end(),
                  [](double& t) { REQUIRE(t == 0.0); });
    REQUIRE(trigon.get_subpower<0>().terms[0] == val);
}

TEST_CASE("construct_component", "Trigon_test")
{
    double val = 7.0;
    size_t index = 3;
    Trigon<double, 2, 6> trigon(val, index);

    REQUIRE(trigon.terms.size() == 21);
    std::for_each(trigon.terms.begin(), trigon.terms.end(),
                  [](double& t) { REQUIRE(t == 0.0); });
    auto& linear_terms = trigon.get_subpower<1>().terms;
    for (size_t i = 0; i < linear_terms.size(); ++i) {
        if (i == index) {
            REQUIRE(linear_terms[i] == 1);
        } else {
            REQUIRE(linear_terms[i] == 0);
        }
    }
    REQUIRE(trigon.get_subpower<0>().terms[0] == val);
}

TEST_CASE("from_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    const double val = 37.0;
    Trigon<double, max_power, dim> trigon(val);

    REQUIRE(trigon.get_subpower<0>().terms[0] == val);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == 0);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == 0);
        }
    }
}

TEST_CASE("get_subpower", "Trigon_test")
{
    const unsigned int max_power = 5;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    REQUIRE(trigon.get_subpower<0>().power() == 0);
    REQUIRE(trigon.get_subpower<1>().power() == 1);
    REQUIRE(trigon.get_subpower<2>().power() == 2);
    REQUIRE(trigon.get_subpower<3>().power() == 3);
    REQUIRE(trigon.get_subpower<4>().power() == 4);
    REQUIRE(trigon.get_subpower<5>().power() == 5);
}

TEST_CASE("get_const_subpower", "Trigon_test")
{
    const unsigned int max_power = 5;
    const unsigned int dim = 6;
    const Trigon<double, max_power, dim> trigon;

    REQUIRE(trigon.get_subpower<0>().power() == 0);
    REQUIRE(trigon.get_subpower<1>().power() == 1);
    REQUIRE(trigon.get_subpower<2>().power() == 2);
    REQUIRE(trigon.get_subpower<3>().power() == 3);
    REQUIRE(trigon.get_subpower<4>().power() == 4);
    REQUIRE(trigon.get_subpower<5>().power() == 5);
}

TEST_CASE("copy", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon);
    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("assignment", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone;
    bygone = trigon;

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("assignment_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    const double val = 37.0;
    trigon = val;
    REQUIRE(trigon.get_subpower<0>().terms[0] == val);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == 0);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == 0);
        }
    }
}

TEST_CASE("plus_equals", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon);

    trigon += bygone;

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            2 * bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == 2 * subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == 2 * subbygone.terms[i]);
        }
    }
}

TEST_CASE("plus_equals_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon);
    const double offset = 37.0;
    trigon += offset;

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0] + offset);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("plus", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon);
    Trigon<double, max_power, dim> saigon;

    saigon = trigon + bygone;

    REQUIRE(saigon.get_subpower<0>().terms[0] ==
            2 * bygone.get_subpower<0>().terms[0]);
    {
        auto& subsaigon = saigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subsaigon.terms.size(); ++i) {
            REQUIRE(subsaigon.terms[i] == 2 * subbygone.terms[i]);
        }
    }
    {
        auto& subsaigon = saigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subsaigon.terms.size(); ++i) {
            REQUIRE(subsaigon.terms[i] == 2 * subbygone.terms[i]);
        }
    }
}

TEST_CASE("plus_power_0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;

    Trigon<double, max_power, dim> bygone(trigon);
    Trigon<double, max_power, dim> saigon;

    saigon = trigon + bygone;

    REQUIRE(saigon.get_subpower<0>().terms[0] ==
            2 * bygone.get_subpower<0>().terms[0]);
}

TEST_CASE("plus_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone;
    const double offset = 37.0;
    bygone = trigon + offset;

    REQUIRE(trigon.get_subpower<0>().terms[0] + offset ==
            bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("plus_double_power_0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;

    Trigon<double, max_power, dim> bygone;
    const double offset = 37.0;
    bygone = trigon + offset;

    REQUIRE(trigon.get_subpower<0>().terms[0] + offset ==
            bygone.get_subpower<0>().terms[0]);
}

TEST_CASE("other_plus_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone;
    const double offset = 37.0;
    bygone = offset + trigon;

    REQUIRE(trigon.get_subpower<0>().terms[0] + offset ==
            bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("double_plus_complex", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<std::complex<double>, max_power, dim> bygone;

    bygone.get_subpower<0>().terms[0] = std::complex<double>(0.0, -888.0);
    std::complex<double> cdummy(0.0, 1.0);
    {
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }
    {
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }

    Trigon<std::complex<double>, max_power, dim> saigon = trigon + bygone;

    // jfa need to check!!!
}

TEST_CASE("complex_plus_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<std::complex<double>, max_power, dim> bygone;

    bygone.get_subpower<0>().terms[0] = std::complex<double>(0.0, -888.0);
    std::complex<double> cdummy(0.0, 1.0);
    {
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }
    {
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }

    Trigon<std::complex<double>, max_power, dim> saigon = bygone + trigon;

    // jfa need to check!!!
}

TEST_CASE("unary_minus", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(-trigon);

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            -bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == -subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == -subbygone.terms[i]);
        }
    }
}

TEST_CASE("minus_equals", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon);
    Trigon<double, max_power, dim> saigon(trigon + trigon + trigon);

    trigon -= saigon;

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            -2 * bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == -2 * subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == -2 * subbygone.terms[i]);
        }
    }
}

TEST_CASE("minus_equals_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon);
    const double offset = 37.0;
    trigon -= offset;

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0] - offset);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("minus", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone(trigon + trigon + trigon);
    Trigon<double, max_power, dim> saigon;

    saigon = trigon - bygone;

    REQUIRE(saigon.get_subpower<0>().terms[0] ==
            -2 * trigon.get_subpower<0>().terms[0]);
    {
        auto& subsaigon = saigon.get_subpower<1>();
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subsaigon.terms.size(); ++i) {
            REQUIRE(subsaigon.terms[i] == -2 * subtrigon.terms[i]);
        }
    }
    {
        auto& subsaigon = saigon.get_subpower<2>();
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subsaigon.terms.size(); ++i) {
            REQUIRE(subsaigon.terms[i] == -2 * subtrigon.terms[i]);
        }
    }
}

TEST_CASE("minus_power_0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;

    Trigon<double, max_power, dim> bygone(trigon + trigon + trigon);
    Trigon<double, max_power, dim> saigon;

    saigon = trigon - bygone;

    REQUIRE(saigon.get_subpower<0>().terms[0] ==
            -2 * trigon.get_subpower<0>().terms[0]);
}

TEST_CASE("minus_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone;
    const double offset = 37.0;
    bygone = trigon - offset;

    REQUIRE(trigon.get_subpower<0>().terms[0] - offset ==
            bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == subbygone.terms[i]);
        }
    }
}

TEST_CASE("minus_double_power_0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;

    Trigon<double, max_power, dim> bygone;
    const double offset = 37.0;
    bygone = trigon - offset;

    REQUIRE(trigon.get_subpower<0>().terms[0] - offset ==
            bygone.get_subpower<0>().terms[0]);
}

TEST_CASE("other_minus_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<double, max_power, dim> bygone;
    const double offset = 37.0;
    bygone = offset - trigon;

    REQUIRE(offset - trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);
    {
        auto& subtrigon = trigon.get_subpower<1>();
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == -subbygone.terms[i]);
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            REQUIRE(subtrigon.terms[i] == -subbygone.terms[i]);
        }
    }
}

TEST_CASE("double_minus_complex", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<std::complex<double>, max_power, dim> bygone;

    bygone.get_subpower<0>().terms[0] = std::complex<double>(0.0, -888.0);
    std::complex<double> cdummy(0.0, 1.0);
    {
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }
    {
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }

    Trigon<std::complex<double>, max_power, dim> saigon = trigon - bygone;

    // jfa need to check!!!
}

TEST_CASE("complex_minus_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 6;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = -999.0;
    double dummy = 1.0;
    {
        auto& subtrigon = trigon.get_subpower<1>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }
    {
        auto& subtrigon = trigon.get_subpower<2>();
        for (size_t i = 0; i < subtrigon.terms.size(); ++i) {
            subtrigon.terms[i] = dummy;
            dummy += 1.0;
        }
    }

    Trigon<std::complex<double>, max_power, dim> bygone;

    bygone.get_subpower<0>().terms[0] = std::complex<double>(0.0, -888.0);
    std::complex<double> cdummy(0.0, 1.0);
    {
        auto& subbygone = bygone.get_subpower<1>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }
    {
        auto& subbygone = bygone.get_subpower<2>();
        for (size_t i = 0; i < subbygone.terms.size(); ++i) {
            subbygone.terms[i] = cdummy;
            cdummy += std::complex<double>(0.0, 1.0);
        }
    }

    Trigon<std::complex<double>, max_power, dim> saigon = bygone - trigon;

    // jfa need to check!!!
}

TEST_CASE("indices_13", "Trigon_test")
{
    Indices_t<1, 3> indices13 = indices<1, 3>();
    REQUIRE(indices13.size() == 3);
    REQUIRE(indices13[0][0] == 0);
    REQUIRE(indices13[1][0] == 1);
    REQUIRE(indices13[2][0] == 2);
}

TEST_CASE("indices_23", "Trigon_test")
{
    Indices_t<2, 3> indices23 = indices<2, 3>();
    REQUIRE(indices23.size() == 6);

    REQUIRE(indices23[0][0] == 0);
    REQUIRE(indices23[0][1] == 0);

    REQUIRE(indices23[1][0] == 0);
    REQUIRE(indices23[1][1] == 1);

    REQUIRE(indices23[2][0] == 0);
    REQUIRE(indices23[2][1] == 2);

    REQUIRE(indices23[3][0] == 1);
    REQUIRE(indices23[3][1] == 1);

    REQUIRE(indices23[4][0] == 1);
    REQUIRE(indices23[4][1] == 2);

    REQUIRE(indices23[5][0] == 2);
    REQUIRE(indices23[5][1] == 2);
}

TEST_CASE("indices_33", "Trigon_test")
{
    Indices_t<3, 3> indices33 = indices<3, 3>();
    REQUIRE(indices33.size() == 10);

    REQUIRE(indices33[0][0] == 0);
    REQUIRE(indices33[0][1] == 0);
    REQUIRE(indices33[0][2] == 0);

    REQUIRE(indices33[1][0] == 0);
    REQUIRE(indices33[1][1] == 0);
    REQUIRE(indices33[1][2] == 1);

    REQUIRE(indices33[2][0] == 0);
    REQUIRE(indices33[2][1] == 0);
    REQUIRE(indices33[2][2] == 2);

    REQUIRE(indices33[3][0] == 0);
    REQUIRE(indices33[3][1] == 1);
    REQUIRE(indices33[3][2] == 1);

    REQUIRE(indices33[4][0] == 0);
    REQUIRE(indices33[4][1] == 1);
    REQUIRE(indices33[4][2] == 2);

    REQUIRE(indices33[5][0] == 0);
    REQUIRE(indices33[5][1] == 2);
    REQUIRE(indices33[5][2] == 2);

    REQUIRE(indices33[6][0] == 1);
    REQUIRE(indices33[6][1] == 1);
    REQUIRE(indices33[6][2] == 1);

    REQUIRE(indices33[7][0] == 1);
    REQUIRE(indices33[7][1] == 1);
    REQUIRE(indices33[7][2] == 2);

    REQUIRE(indices33[8][0] == 1);
    REQUIRE(indices33[8][1] == 2);
    REQUIRE(indices33[8][2] == 2);

    REQUIRE(indices33[9][0] == 2);
    REQUIRE(indices33[9][1] == 2);
    REQUIRE(indices33[9][2] == 2);
}

TEST_CASE("fill_index_to_canonical", "Trigon_test")
{
    auto& map = fill_index_to_canonical<4, 2>();

    REQUIRE((map.at({ { 0, 0, 0, 0 } })) == 0);
    REQUIRE((map.at({ { 0, 0, 0, 1 } })) == 1);
    REQUIRE((map.at({ { 0, 0, 1, 1 } })) == 2);
    REQUIRE((map.at({ { 0, 1, 1, 1 } })) == 3);
}

TEST_CASE("index_to_canonical", "Trigon_test")
{
    REQUIRE((index_to_canonical<4, 2>().at({ { 0, 0, 0, 0 } })) == 0);
    REQUIRE((index_to_canonical<4, 2>().at({ { 0, 0, 0, 1 } })) == 1);
    REQUIRE((index_to_canonical<4, 2>().at({ { 0, 0, 1, 1 } })) == 2);
    REQUIRE((index_to_canonical<4, 2>().at({ { 0, 1, 1, 1 } })) == 3);
}

TEST_CASE("times_equal01", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.terms[0] = 2.0;
    bygone.terms[0] = 3.0;
    trigon *= bygone;

    REQUIRE(trigon.terms[0] == 6.0);
}

TEST_CASE("times_equal05", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 5;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.terms[0] = 2.0;
    bygone.terms[0] = 3.0;
    trigon *= bygone;

    REQUIRE(trigon.terms[0] == 6.0);
}

TEST_CASE("times_equal1", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms[0] = 3.0;
    bygone.get_subpower<0>().terms[0] = 5.0;
    bygone.terms[0] = 7.0;
    trigon *= bygone;

    REQUIRE(trigon.get_subpower<0>().terms[0] == 10.0);
    REQUIRE(trigon.terms[0] == 29.0);
}

TEST_CASE("times_equal12", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms[0] = 3.0;
    trigon.terms[1] = 5.0;
    bygone.get_subpower<0>().terms[0] = 7.0;
    bygone.terms[0] = 11.0;
    bygone.terms[1] = 13.0;
    trigon *= bygone;

    REQUIRE(trigon.get_subpower<0>().terms[0] == 14.0);
    REQUIRE(trigon.terms[0] == 43.0);
    REQUIRE(trigon.terms[1] == 61.0);
}

TEST_CASE("times_equal22", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;
    bygone.get_subpower<0>().terms[0] = 17.0;
    bygone.get_subpower<1>().terms[0] = 19.0;
    bygone.get_subpower<1>().terms[1] = 23.0;
    bygone.terms[0] = 29.0;
    bygone.terms[1] = 31.0;
    bygone.terms[2] = 37.0;
    trigon *= bygone;

    REQUIRE(trigon.get_subpower<0>().terms[0] == (2 * 17));
    REQUIRE(trigon.get_subpower<1>().terms[0] == (2 * 19 + 17 * 3));
    REQUIRE(trigon.get_subpower<1>().terms[1] == (2 * 23 + 17 * 5));
    REQUIRE(trigon.terms[0] == (2 * 29 + 3 * 19 + 7 * 17));
    REQUIRE(trigon.terms[1] == (2 * 31 + 3 * 23 + 5 * 19 + 11 * 17));
    REQUIRE(trigon.terms[2] == (2 * 37 + 5 * 23 + 13 * 17));
}

TEST_CASE("times_equal_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    Trigon<double, max_power, dim> bygone(trigon);
    const double mult = 17.0;
    bygone *= mult;

    REQUIRE(bygone.get_subpower<0>().terms[0] ==
            trigon.get_subpower<0>().terms[0] * mult);
    REQUIRE(bygone.get_subpower<1>().terms[0] ==
            trigon.get_subpower<1>().terms[0] * mult);
    REQUIRE(bygone.get_subpower<1>().terms[1] ==
            trigon.get_subpower<1>().terms[1] * mult);
    REQUIRE(bygone.terms[0] == trigon.terms[0] * mult);
    REQUIRE(bygone.terms[1] == trigon.terms[1] * mult);
    REQUIRE(bygone.terms[2] == trigon.terms[2] * mult);
}

TEST_CASE("times01", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.terms[0] = 2.0;
    bygone.terms[0] = 3.0;

    Trigon<double, max_power, dim> saigon(trigon * bygone);

    REQUIRE(saigon.terms[0] == 6.0);
}

TEST_CASE("times12", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms[0] = 3.0;
    trigon.terms[1] = 5.0;
    bygone.get_subpower<0>().terms[0] = 7.0;
    bygone.terms[0] = 11.0;
    bygone.terms[1] = 13.0;

    Trigon<double, max_power, dim> saigon(trigon * bygone);

    REQUIRE(saigon.get_subpower<0>().terms[0] == 14.0);
    REQUIRE(saigon.terms[0] == 43.0);
    REQUIRE(saigon.terms[1] == 61.0);
}

TEST_CASE("double_times_complex", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    Trigon<std::complex<double>, max_power, dim> bygone;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms[0] = 3.0;
    trigon.terms[1] = 5.0;
    bygone.get_subpower<0>().terms[0] = std::complex<double>(0.0, 7.0);
    bygone.terms[0] = std::complex<double>(0.0, 11.0);
    bygone.terms[1] = std::complex<double>(0.0, 13.0);

    Trigon<std::complex<double>, max_power, dim> saigon(trigon * bygone);

    // jfa need to check!!!
}

TEST_CASE("complex_times_double", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    Trigon<std::complex<double>, max_power, dim> bygone;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms[0] = 3.0;
    trigon.terms[1] = 5.0;
    bygone.get_subpower<0>().terms[0] = std::complex<double>(0.0, 7.0);
    bygone.terms[0] = std::complex<double>(0.0, 11.0);
    bygone.terms[1] = std::complex<double>(0.0, 13.0);

    Trigon<std::complex<double>, max_power, dim> saigon(bygone * trigon);

    // jfa need to check!!!
}

TEST_CASE("times_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    const double mult = 17.0;
    Trigon<double, max_power, dim> bygone(trigon * mult);

    REQUIRE(bygone.get_subpower<0>().terms[0] ==
            trigon.get_subpower<0>().terms[0] * mult);
    REQUIRE(bygone.get_subpower<1>().terms[0] ==
            trigon.get_subpower<1>().terms[0] * mult);
    REQUIRE(bygone.get_subpower<1>().terms[1] ==
            trigon.get_subpower<1>().terms[1] * mult);
    REQUIRE(bygone.terms[0] == trigon.terms[0] * mult);
    REQUIRE(bygone.terms[1] == trigon.terms[1] * mult);
    REQUIRE(bygone.terms[2] == trigon.terms[2] * mult);
}

TEST_CASE("times_double0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.terms[0] = 2.0;

    const double mult = 17.0;
    Trigon<double, max_power, dim> bygone(trigon * mult);

    REQUIRE(bygone.terms[0] == 2.0 * mult);
}

TEST_CASE("times_double_other", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    const double mult = 17.0;
    Trigon<double, max_power, dim> bygone(mult * trigon);

    REQUIRE(bygone.get_subpower<0>().terms[0] ==
            trigon.get_subpower<0>().terms[0] * mult);
    REQUIRE(bygone.get_subpower<1>().terms[0] ==
            trigon.get_subpower<1>().terms[0] * mult);
    REQUIRE(bygone.get_subpower<1>().terms[1] ==
            trigon.get_subpower<1>().terms[1] * mult);
    REQUIRE(bygone.terms[0] == trigon.terms[0] * mult);
    REQUIRE(bygone.terms[1] == trigon.terms[1] * mult);
    REQUIRE(bygone.terms[2] == trigon.terms[2] * mult);
}

TEST_CASE("times_double_other0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.terms[0] = 2.0;

    const double mult = 17.0;
    Trigon<double, max_power, dim> bygone(mult * trigon);

    REQUIRE(bygone.terms[0] == 2.0 * mult);
}

TEST_CASE("times_complex", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    const std::complex<double> complex_i(0.0, 1.0);
    Trigon<std::complex<double>, max_power, dim> bygone(complex_i * trigon);

    // jfa: missing comparison!!!
    // mising other mult!!!
}

TEST_CASE("partial_deriv12", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms = { { 3.0, 5.0 } };

    auto bygone = partial_deriv(trigon, 0);
    REQUIRE(bygone.terms[0] == 3.0);
    auto saigon = partial_deriv(trigon, 1);
    REQUIRE(saigon.terms[0] == 5.0);
}

TEST_CASE("partial_deriv31", "Trigon_test")
{
    const unsigned int max_power = 3;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<2>().terms[0] = 5.0;
    trigon.terms[0] = 7.0;

    auto bygone = partial_deriv(trigon, 0);
    REQUIRE(bygone.get_subpower<0>().terms[0] == 3.0);
    REQUIRE(bygone.get_subpower<1>().terms[0] == 10.0);
    REQUIRE(bygone.terms[0] == 21.0);
}

TEST_CASE("partial_deriv22", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms = { { 3.0, 5.0 } };
    trigon.terms = { { 7.0, 11.0, 13.0 } };

    auto bygone = partial_deriv(trigon, 0);
    REQUIRE(bygone.get_subpower<0>().terms[0] == 3.0);
    REQUIRE(bygone.get_subpower<1>().terms[0] == 14.0);
    REQUIRE(bygone.get_subpower<1>().terms[1] == 11.0);

    auto saigon = partial_deriv(trigon, 1);
    REQUIRE(saigon.get_subpower<0>().terms[0] == 5.0);
    REQUIRE(saigon.get_subpower<1>().terms[0] == 11.0);
    REQUIRE(saigon.get_subpower<1>().terms[1] == 26.0);
}

TEST_CASE("div_equal0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;
    trigon.terms[0] = 6.0;

    Trigon<double, max_power, dim> bygone;
    bygone.terms[0] = 2.0;

    trigon /= bygone;

    REQUIRE(trigon.terms[0] == 3.0);
}

TEST_CASE("div_equal13", "Trigon_test")
{
    const unsigned int max_power = 1;
    const unsigned int dim = 3;
    Trigon<double, max_power, dim> trigon;
    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.terms = { { 3.0, 5.0, 7.0 } };

    Trigon<double, max_power, dim> bygone;
    bygone.get_subpower<0>().terms[0] = 11.0;
    bygone.terms = { { 13.0, 17.0, 19.0 } };

    Trigon<double, max_power, dim> saigon(trigon * bygone);

    saigon /= trigon;

    REQUIRE(saigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);

    REQUIRE(saigon.terms[0] == bygone.terms[0]);
    REQUIRE(saigon.terms[1] == bygone.terms[1]);
    REQUIRE(saigon.terms[2] == bygone.terms[2]);
}

TEST_CASE("div_equal22", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms = { { 3.0, 5.0 } };
    trigon.terms = { { 7.0, 11.0, 13.0 } };

    Trigon<double, max_power, dim> bygone;
    bygone.get_subpower<0>().terms[0] = 17.0;
    bygone.get_subpower<1>().terms = { { 19.0, 23.0 } };
    bygone.terms = { { 29.0, 31.0, 37.0 } };

    Trigon<double, max_power, dim> saigon(trigon * bygone);

    saigon /= trigon;

    REQUIRE(saigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);

    REQUIRE(saigon.get_subpower<1>().terms[0] ==
            bygone.get_subpower<1>().terms[0]);
    REQUIRE(saigon.get_subpower<1>().terms[1] ==
            bygone.get_subpower<1>().terms[1]);

    REQUIRE(saigon.terms[0] == bygone.terms[0]);
    REQUIRE(saigon.terms[1] == bygone.terms[1]);
    REQUIRE(saigon.terms[2] == bygone.terms[2]);
}

TEST_CASE("div01", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;
    Trigon<double, max_power, dim> bygone;

    trigon.terms[0] = 6.0;
    bygone.terms[0] = 3.0;

    Trigon<double, max_power, dim> saigon(trigon / bygone);

    REQUIRE(saigon.terms[0] == 2.0);
}

TEST_CASE("div_22", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;
    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms = { { 3.0, 5.0 } };
    trigon.terms = { { 7.0, 11.0, 13.0 } };

    Trigon<double, max_power, dim> bygone;
    bygone.get_subpower<0>().terms[0] = 17.0;
    bygone.get_subpower<1>().terms = { { 19.0, 23.0 } };
    bygone.terms = { { 29.0, 31.0, 37.0 } };

    Trigon<double, max_power, dim> saigon(trigon * bygone);

    Trigon<double, max_power, dim> hochiminh(saigon / trigon);

    REQUIRE(hochiminh.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);

    REQUIRE(hochiminh.get_subpower<1>().terms[0] ==
            bygone.get_subpower<1>().terms[0]);
    REQUIRE(hochiminh.get_subpower<1>().terms[1] ==
            bygone.get_subpower<1>().terms[1]);

    REQUIRE(hochiminh.terms[0] == bygone.terms[0]);
    REQUIRE(hochiminh.terms[1] == bygone.terms[1]);
    REQUIRE(hochiminh.terms[2] == bygone.terms[2]);
}

TEST_CASE("div_double", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    const double mult = 17.0;
    trigon.get_subpower<0>().terms[0] = 2.0 * mult;
    trigon.get_subpower<1>().terms[0] = 3.0 * mult;
    trigon.get_subpower<1>().terms[1] = 5.0 * mult;
    trigon.terms[0] = 7.0 * mult;
    trigon.terms[1] = 11.0 * mult;
    trigon.terms[2] = 13.0 * mult;

    Trigon<double, max_power, dim> bygone(trigon / mult);

    REQUIRE(bygone.get_subpower<0>().terms[0] ==
            trigon.get_subpower<0>().terms[0] / mult);
    REQUIRE(bygone.get_subpower<1>().terms[0] ==
            trigon.get_subpower<1>().terms[0] / mult);
    REQUIRE(bygone.get_subpower<1>().terms[1] ==
            trigon.get_subpower<1>().terms[1] / mult);
    REQUIRE(bygone.terms[0] == trigon.terms[0] / mult);
    REQUIRE(bygone.terms[1] == trigon.terms[1] / mult);
    REQUIRE(bygone.terms[2] == trigon.terms[2] / mult);
}

TEST_CASE("div_double0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.terms[0] = 34.0;

    const double div = 17.0;
    Trigon<double, max_power, dim> bygone(trigon / div);

    REQUIRE(bygone.terms[0] == 34.0 / div);
}

TEST_CASE("div_double_other", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    const double mult = 17.0;
    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    Trigon<double, max_power, dim> bygone(mult / trigon);
    Trigon<double, max_power, dim> saigon(trigon * bygone);

    REQUIRE(saigon.get_subpower<0>().terms[0] == mult);
    REQUIRE(saigon.get_subpower<1>().terms[0] == 0);
    REQUIRE(saigon.get_subpower<1>().terms[1] == 0);
    REQUIRE(saigon.terms[0] == 0);
    REQUIRE(saigon.terms[1] == 0);
    REQUIRE(saigon.terms[2] == 0);
}

TEST_CASE("div_double_other0", "Trigon_test")
{
    const unsigned int max_power = 0;
    const unsigned int dim = 1;
    Trigon<double, max_power, dim> trigon;

    trigon.terms[0] = 17.0;

    const double val = 34.0;
    Trigon<double, max_power, dim> bygone(val / trigon);

    REQUIRE(bygone.terms[0] == val / 17.0);
}

TEST_CASE("real", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    const double mult = 17.0;
    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    Trigon<double, max_power, dim> bygone(
        real(std::complex<double>(1.0, 0.0) * trigon));

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);
    REQUIRE(trigon.get_subpower<1>().terms[0] ==
            bygone.get_subpower<1>().terms[0]);
    REQUIRE(trigon.get_subpower<1>().terms[1] ==
            bygone.get_subpower<1>().terms[1]);
    REQUIRE(trigon.terms[0] == bygone.terms[0]);
    REQUIRE(trigon.terms[1] == bygone.terms[1]);
    REQUIRE(trigon.terms[2] == bygone.terms[2]);
}

TEST_CASE("imag", "Trigon_test")
{
    const unsigned int max_power = 2;
    const unsigned int dim = 2;
    Trigon<double, max_power, dim> trigon;

    const double mult = 17.0;
    trigon.get_subpower<0>().terms[0] = 2.0;
    trigon.get_subpower<1>().terms[0] = 3.0;
    trigon.get_subpower<1>().terms[1] = 5.0;
    trigon.terms[0] = 7.0;
    trigon.terms[1] = 11.0;
    trigon.terms[2] = 13.0;

    Trigon<double, max_power, dim> bygone(
        imag(std::complex<double>(0.0, 1.0) * trigon));

    REQUIRE(trigon.get_subpower<0>().terms[0] ==
            bygone.get_subpower<0>().terms[0]);
    REQUIRE(trigon.get_subpower<1>().terms[0] ==
            bygone.get_subpower<1>().terms[0]);
    REQUIRE(trigon.get_subpower<1>().terms[1] ==
            bygone.get_subpower<1>().terms[1]);
    REQUIRE(trigon.terms[0] == bygone.terms[0]);
    REQUIRE(trigon.terms[1] == bygone.terms[1]);
    REQUIRE(trigon.terms[2] == bygone.terms[2]);
}
